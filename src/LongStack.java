import java.util.LinkedList;

/**
 * The class is destined for the homework assignment no 3.
 * It takes in a Reverse Polish Notation (RPN) string, checks it
 * for the errata and attempts to solve mathematically.
 * The skeleton is attributed to jpoial.
 *  - https://bitbucket.org/itc_algorithms/home3/
 *  @author Igavene tudeng jeekim616
 *  his previous incarnation:  https://bitbucket.org/furunkel027/kodu3/
 *  in 2015, furunkel027 got the inspiration from there:
 *     - http://enos.itcollege.ee/~ylari/I231/Intstack.java
 *  At last that made clear what the task actually means.
 */
public class LongStack {
    // PLZ look for the main() near the very end (only used for testing)

    private LinkedList<Long> lifo; // MAX_VALUE 2E63-1

    public LongStack() {
        lifo = new LinkedList<Long>();
    } // OOP sõjakoledus nimega konstruktor

    // --------------------------------
    @Override
    public Object clone() throws CloneNotSupportedException {
        LongStack tmp = new LongStack();
        tmp.lifo = (LinkedList<Long>) lifo.clone();
        return tmp;
    }

    // =======================================================
    // Siitmaalt algavad etteantud avalikud FUNKTSIOONID

    // -------------------- Is the Stack Empty?
    public boolean stEmpty() {
        boolean empty = false;
        if (lifo.isEmpty()) {
            empty = true;
        }
        return empty;
    }

    // --------------------- Size
    public int size() {
        return lifo.size();
    } // Formalism kuubis.

    // --------------------- Lükka järjekordne element pinusse
    public void push (long a) {
        lifo.push(a);
        return;
    }

    // ---------------------- Tõmba järjekordne element pinust
    public long pop() {
        long item = 0L;
        if (lifo.size() == 0) {
            throw new RuntimeException("EXCEPTION: Zero operand in well to proceed with the pop()");
        }
        item = lifo.pop();
        return item;
    }

    // ---------------------- Top of the Stack?
    public long tos() {
        long item = 0L;
        if (lifo.size() == 0) {
            throw new RuntimeException("EXCEPTION: Zero operand in well to proceed with the Top of Stack tos()");
        }
        item = lifo.getFirst();
        return item;
    }

    // ---------------------- Tiri kaks argumenti ja saa tehe, soorita tehe, lükka vastus tagasi.
    // NB! tehtekäskusid ei hoita stackis vaid eraldi muutujas
    public void op(String instruction) {
        // First catch errors related to the instructions
       if (instruction.isEmpty()) {
           throw new RuntimeException("EXCEPTION at op: a missing opCode.");
       }
       if (instruction.length() > 1) {
           throw new RuntimeException(
                   "EXCEPTION at opCode _" + instruction + "_: multichar opCodes (like \"log\" or \"sin\") not yet implemented");
       }
       if (!isKosherOp(instruction)) {
           throw new RuntimeException(
                   "EXCEPTION at opCode: Only these ops permitted: _+-*/_ but not this: _"
                           + instruction + "_ .");
       }
       char attemptedOp = instruction.charAt(0); // cheaper to play with char than String

        // Then catch errors related to the stack of arguments
        if (lifo.size() < 2) {
            throw new RuntimeException("EXCEPTION with op " + attemptedOp + ": The well does not contain the requested two operands");
        }

       // Now it's safe with the actual calculations
       long result = 0;
       long operand1 = lifo.remove(1);
       long operand2 = lifo.remove(0);

       if (attemptedOp == '+') {
           result = operand1 + operand2;
       }
       if (attemptedOp == '-') {
           result = operand1 - operand2;
       }
       if (attemptedOp == '*') {
           result = operand1 * operand2;
       }
       if (attemptedOp == '/') {
           result = operand1 / operand2;
       }
       lifo.push(result);
       return;
   }


    // -------------------- Võrdlus läbi istmiku
   @Override
   public boolean equals(Object DuT) {
       if (DuT instanceof LongStack) {
           LinkedList<Long> tmp = (LinkedList<Long>) ((LongStack) DuT).lifo; // !!
           if (lifo.size() == tmp.size()) {
               for (int i = 0; i < lifo.size(); i++)
                   if (lifo.get(i) != tmp.get(i))
                       return false;
               return true;
           }
       }
       return false;
   }

    // -------------------- toString avalik meetod
   @Override
   public String toString() {
       StringBuilder conveyer = new StringBuilder();
       Long item;
       for (int i = (lifo.size() - 1); i >= 0; i--) {
           item = lifo.get(i);
           conveyer.append(item);
           if (i != 0) // v.a. päramisel korral
               conveyer.append(" ");
       }
       return conveyer.toString();
   }

    // ----------------------------------------------------------

    /**
     * The input String with the math expression to solve.
     * the output with the result, or, if undoable, yelling loud
     *
     * @param pol
     * @return
     */
    public static long interpret(String pol) {
        // Siin paikneb selle proge süda ja üldse mitte main() kandis
        String rpnInputString = pol;
        int charCount = rpnInputString.length();
        if ((charCount == 0) || rpnInputString == null) {
            System.out.println("\nERROR: a NULL/NIL/0 input string, RPN=|<NOTHING>|.");
            throw new RuntimeException(
                    "EXCEPTION in interpret(): No arguments or opcodes present."); // no pol string ;)
        }

        // tundub, et testid ei kontrolli ülearuseid tühikuid sees ja äärtes. Võiksivad.
        rpnInputString = rpnInputString.trim().replaceAll("\\s+", " ");

        String[] parsedTokens = rpnInputString.trim().split("\\s+");
        int wordCount = parsedTokens.length;

        // ToDo: võimalik alternatiivne loogika: kui pole vähemalt 2 arvu ja 1 tehet

        // Siin me tekitame endale kaevu, kuhu asju pistma hakata.
        LongStack deepWell = new LongStack();

        int arguments = 0;
        int operators = 0;

        for (String token : parsedTokens) {
            // ----- 1 -----------------
            // Scenario 1: sisendis on number, lisame patta
            if (isNumeric(token)) {
                try {
                    deepWell.push(Long.valueOf(token)); // Pistame numbri kaevu
                    arguments = arguments + 1;
                } catch (NumberFormatException e) {
                    // Do Nothing, mask the exception
                }

            } else {
                // ------ 2 -------------
                // Scenario 2: sisendis on hoopis tehtemärk
                String tehe = token;
                if (!isKosherOp((tehe))) {
                    System.out.println("\nERROR: Illegal OpCode: _" + tehe + "_ during RPN=|" + pol + "|.");
                    throw new RuntimeException(
                            "EXCEPTION in interpret phase 2: Not a number, not an OpCode (+-*/): _"
                                    + tehe
                                    + "_ We are unable to continue.");
                    // ja väljalend
                }
                // DEBUG System.out.print(tehe + " -> validOpCode; ");

                // ----- 3 -------------
                // Scenario 3: encounter no operands case
                if (deepWell.stEmpty()) {
                    System.out.println("\nERROR: for Opcode: _" + tehe + "_ yet no operands in stack during RPN=|" + pol + "|.");
                    throw new RuntimeException(
                            "EXCEPTION in interpret phase 3: OpCode _"
                                    + tehe
                                    + "_ is OK but there are 0 arguments for it in the well.");
                } else {
                    // ------ 4 ------------------
                    // Scenario 4: encounter one missing operand case.
                    if (deepWell.size() < 2) {
                        System.out.println("\nERROR: for Opcode: _" + tehe + "_ only one operand in stack, 2 needed, during RPN=|" + pol + "|.");
                        throw new RuntimeException(
                                "EXCEPTION in interpret phase 4: Only one argument in the well for a two-arg Opcode.");
                    } // Scenario 4
                } // Scenario 3
                // Scenario 5: everything OK: two operands and one opcode available.
                deepWell.op(token);
                operators++;

            } // endIF
        } // endFOR

        if ((arguments - 1) != operators) { // a somewhat artificial condition
            System.out.println("\nERROR: ArgCount=" + arguments + " found. ArgCount=(OpCount+1)=" + (operators + 1) + " expected, during RPN=|" + pol + "|.");
            throw new RuntimeException(
                    "EXCEPTION at interpret final phase - Stack is out of balance (integers: "
                            + arguments + ", operators: " + operators + ").");
        }
        return deepWell.pop();
    } // interpret

    // Sihukesed sõbralikud abifunktsioonid

    // --------------------------------------
    public static boolean isNumeric(String str) {
        // Inspiratsiooni saamise kohad:
        // http://stackoverflow.com/questions/1102891/how-to-check-if-a-string-is-a-numeric-type-in-java
        // return str.matches("[+-]?\\d*(\\.\\d+)?");
        // http://stackoverflow.com/questions/3507162/most-elegant-isnumeric-solution-for-java
        if (str.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+")) {
            return true;
        } else {
            return false;
        }
    }

    // --------------------------------------
    public static boolean isKosherOp(String str) {
        char tehe = str.charAt(0);
        if ((tehe == '+') || (tehe == '-') || (tehe == '*') || (tehe == '/')) {
            return true;
        }
        return false;
    }


    // ============================ MAIN ======
   public static void main (String[] argum) {
        /* DEBUG
       LongStack test = new LongStack();
       System.out.println("trace 01 | lifo.size=" + test.size + " | " + test.toString());
       test.push(17L);
       System.out.println("trace 02 | lifo.size=" + test.size + " | " + test.toString());
       test.push(53L);
       System.out.println("trace 03 | lifo.size=" + test.size + " | " + test.toString());
       test.push(66L);
       System.out.println("trace 04 | lifo.size=" + test.size + " | " + test.toString());
       test.push(24L);
       System.out.println("trace 05 | lifo.size=" + test.size + " | " + test.toString());
       test.op("/");
       System.out.println("trace 06 | lifo.size=" + test.size + " | " + test.toString());
       test.op("*");
       System.out.println("trace 07 | lifo.size=" + test.size + " | " + test.toString());
       test.op("-");
       System.out.println("trace 08 | lifo.size=" + test.size + " | " + test.toString());
       System.out.println(" Aga eksole, tehtemärgid küll ei lähe Long-tüüpi virna.");
       System.out.println("Katse: tirime ühe elemendi tagasi");
       long tulemus = test.pop();
       System.out.println("trace 09 | lifo.size=" + test.size + " | " + test.toString());
       System.out.println("   tulemus A: " + tulemus);

       System.out.println("Kopeerime...");
       LongStack koopia = test;
       System.out.println("   Virnad on võrdsed? " + koopia.equals(test));
       System.out.println("      algne:  " + test);
       System.out.println("      koopia: " + koopia);

       System.out.println("Kloonime...");
       try {
           koopia = (LongStack) test.clone();
       } catch (CloneNotSupportedException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       }
       System.out.println("   Virnad on võrdsed? " + koopia.equals(test));
       System.out.println("      algne:  " + test);
       System.out.println("      koopia: " + koopia);

       System.out.println("Lisame kummagisse virna erinevad elemendid...");
       test.push(6);
       koopia.push(3);
       System.out.println("   Virnad on võrdsed? " + koopia.equals(test));
       System.out.println("      algne:  " + test);
       System.out.println("      koopia: " + koopia);

       System.out.println("Tirime ühest virnast ühe elemendi välja...");
       koopia.pop();
       System.out.println("   Virnad on võrdsed? " + koopia.equals(test));
       System.out.println("      algne:  " + test);
       System.out.println("      koopia: " + koopia);

       System.out.print("The result: " + interpret("   5  10 -4  2 -1  / + * -  ") + "\n");
       System.out.print("The result: " + interpret(" 15 7 1 1 + - / 3 * 2 1 1 + + - ") + "\n");
       */
   }
}

